import dicom, lmdb, cv2, re, sys
import os, fnmatch, shutil, subprocess
from os.path import basename
import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
np.random.seed(2017)


DATA_ROOT_PATH = "../input/york"
TRAIN_IMG_PATH = os.path.join(DATA_ROOT_PATH,"mrimages")
TRAIN_CONTOUR_PATH = os.path.join(DATA_ROOT_PATH,"manual_seg")


def load_contour_multi_ch(imgfile, contour_path, input_C, input_H, input_W, bROI=False, hR=64):     
   
    filename = os.path.splitext(basename(imgfile))[0]    
    img_name = filename
    seg_name = filename+'_seg'
    filename = filename + '_seg.mat'
    ctrfile = os.path.join(contour_path, filename)

    data = sio.loadmat(imgfile)
    label =  sio.loadmat(ctrfile)
    arrImg = data['sol_yxzt']   #256 256 12 20
    arrLab = label['manual_seg_32points'] #12 20 
    
    imgs,labs = [],[]    
    
 
    for i in range(arrImg.shape[2]):
        bFull = True
        for jj in range(arrImg.shape[3]):
            lab = arrLab[i,jj]
            if lab.shape[0]<2:
                bFull = False                              
        #print 'full cycle? ', bFull
        if not bFull:            
            continue
        for j in range(arrImg.shape[3]):
            lab = arrLab[i,j]
            if lab.shape[0]>1:                
                img = []
				
                input_offset = input_C/2
                #print "\nload_contour_multi_ch input_offset : ", input_offset, "  for j : ", j
                
		#Append the before images
                n = input_offset			
                while (n > 0):
                    pos = j - n
                    if pos < 0:
                        pos = (20 - n)+j
                    #print "before position : ", pos	
                    imgBefore = arrImg[:,:,i, pos]	
                    img.append(imgBefore)					
                    n = n-1
                
                #print "current position : ", j
                imgCurr = arrImg[:,:,i,j]
                img.append(imgCurr)
				
                #append the after images
                n = 0
                while (n < input_offset):
                    pos = j + (n+1)
                    if pos > 19:
                        pos = pos - 20
                    #print "after position : ", pos
                    imgAfter = arrImg[:,:,i, pos]	
                    img.append(imgAfter)	
                    n = n+1
				
                img = np.array(img)
                img = img.transpose(1,2,0)   #[H,W,C]
    
                lab = lab.astype(np.int32)
                lab = lab.reshape(lab.shape[0],1,lab.shape[1])
                lab[32,:,:] = lab[0,:,:] #copy 0 to close loop
                lab = np.append(lab,[lab[33,:,:]],0) #add 65 to close loop              
                lab = lab[0:33] #for cavity only
                mask = np.zeros(img.shape[:2])
                mask = mask.astype(np.uint8)
                cv2.fillPoly(mask,[lab],1)
                

                if bROI:

                    xMin = np.min(lab[:,:,0])
                    xMax = np.max(lab[:,:,0])
                    yMin = np.min(lab[:,:,1])
                    yMax = np.max(lab[:,:,1])  
                
                    xCnt = (xMin+xMax)/2
                    yCnt = (yMin+yMax)/2
                

    		    Rec = mask[yCnt-hR:yCnt+hR,xCnt-hR:xCnt+hR]
                    small = img[yCnt-hR:yCnt+hR,xCnt-hR:xCnt+hR]
                    img = small
                    mask = Rec 



                #import pdb; pdb.set_trace()
                img = cv2.resize(img,(input_H,input_W))
                label = cv2.resize(mask,(input_H,input_W)) 
	

		img = img.astype(np.int64)   

                vMean = np.mean(img)
                vVar = np.std(img)
                img = (img - vMean)*1.0 / vVar

                if (input_C!=1):
                    img = img.transpose(2, 0, 1)  #[C,H,W]
                imgs.append(img)
                labs.append(label)


    return np.array(imgs), np.array(labs)


def Load_York_Data(input_C, input_H, input_W, bROI=False, hR=64):

    arrlist = [os.path.join(dirpath, f)
        for dirpath, dirnames, files in os.walk(TRAIN_IMG_PATH)
        for f in fnmatch.filter(files, '*.mat')]
    nTest = 6
    np.random.shuffle(arrlist)
    tstlist = arrlist[:nTest]
    #print 'test list: ', tstlist
    trnlist = arrlist[nTest:]
    #print 'trnlist list: ', trnlist

    train_imglist = trnlist
    test_imglist = tstlist
    
 
    n_samples = 5011
    print 'trnlist list: ', trnlist
    X_train1 = np.zeros((n_samples, input_C, input_H, input_W))
    Y_train1 = np.zeros((n_samples, 1      , input_H, input_W)) 
    count = 0
    for imglist in train_imglist:  
        arrImg, arrLabel = load_contour_multi_ch(imglist, TRAIN_CONTOUR_PATH, input_C, input_H, input_W, bROI, hR)	          

        if input_C==1:
            arrImg = np.expand_dims(arrImg, axis=1)
        
        arrLabel = np.expand_dims(arrLabel, axis=1)

        X_train1[count:count+len(arrImg), :, :, :] = arrImg 
        Y_train1[count:count+len(arrImg), :, :, :] = arrLabel
        count = count + len(arrImg)
        #print("samples number (count) = {:d}".format(count))

    #import pdb; pdb.set_trace()
    X_train = X_train1[:count]
    Y_train = Y_train1[:count]

    print 'test list: ', tstlist
    X_test1 = np.zeros((n_samples, input_C, input_H, input_W))
    Y_test1 = np.zeros((n_samples, 1      , input_H, input_W)) 
    count = 0
    for imglist in test_imglist:  
        arrImg, arrLabel = load_contour_multi_ch(imglist, TRAIN_CONTOUR_PATH, input_C, input_H, input_W, bROI, hR)	          

        if input_C==1:
            arrImg = np.expand_dims(arrImg, axis=1)
        
        arrLabel = np.expand_dims(arrLabel, axis=1)

        X_test1[count:count+len(arrImg), :, :, :] = arrImg 
        Y_test1[count:count+len(arrImg), :, :, :] = arrLabel
        count = count + len(arrImg)
        #print("samples number (count) = {:d}".format(count))

    #import pdb; pdb.set_trace()
    X_test = X_test1[:count]
    Y_test = Y_test1[:count]

 
    return X_train, Y_train, X_test, Y_test



def load_loc_annotation(imgfile, contour_path, input_C, input_H, input_W, bTrain=True):     
   
    filename = os.path.splitext(basename(imgfile))[0]    
    img_name = filename
    seg_name = filename+'_seg'
    filename = filename + '_seg.mat'
    ctrfile = os.path.join(contour_path, filename)

    data = sio.loadmat(imgfile)
    label =  sio.loadmat(ctrfile)
    arrImg = data['sol_yxzt']   #256 256 12 20
    arrLab = label['manual_seg_32points'] #12 20 
    
    imgs,x1,x2,y1,y2 = [],[],[],[],[]
    
    for i in range(arrImg.shape[2]):
        for j in range(arrImg.shape[3]):
            lab = arrLab[i,j]
            if lab.shape[0]>1:                
                img = arrImg[:,:,i,j]  		
                #import pdb; pdb.set_trace()
                lab = lab.astype(np.int32)
                lab = lab.reshape(lab.shape[0],1,lab.shape[1])
                lab[32,:,:] = lab[0,:,:] #copy 0 to close loop
                lab = np.append(lab,[lab[33,:,:]],0) #add 65 to close loop                              
                
                org_H, org_W = img.shape[0:2]

                xFactor = input_W*1.0/org_W 
                yFactor = input_H*1.0/org_H 
                #print xFactor, yFactor
                x1.append(min(lab[:,0,0])*xFactor)
                x2.append(max(lab[:,0,0])*xFactor)
                
                y1.append(min(lab[:,0,1])*yFactor)
                y2.append(max(lab[:,0,1])*yFactor)
                
                img = cv2.resize(img,(input_H,input_W))
                 
		img = img.astype(np.int64)   

                vMean = np.mean(img)
                vVar = np.std(img)
                img = (img - vMean)*1.0 / vVar

                
                imgs.append(img)
                


    return np.array(imgs), np.array(x1), np.array(x2), np.array(y1), np.array(y2)



def Load_Loc_Data(input_C, input_H, input_W, bTrain=False):

    arrlist = [os.path.join(dirpath, f)
        for dirpath, dirnames, files in os.walk(TRAIN_IMG_PATH)
        for f in fnmatch.filter(files, '*.mat')]
    nTest = 6
    np.random.shuffle(arrlist)
    tstlist = arrlist[:nTest]
    trnlist = arrlist[nTest:]
 
    train_imglist = trnlist
    test_imglist = tstlist
    
    if bTrain:
        arr_image_list = train_imglist
    else: 
        arr_image_list = test_imglist  
  
    n_samples = 5011
    
    imgs, x1, x2, y1, y2 = [], [], [], [], []

    count = 0
    for imglist in arr_image_list:  
        arrImg, arrX1, arrX2, arrY1, arrY2 = load_loc_annotation(imglist, TRAIN_CONTOUR_PATH, input_C, input_H, input_W, bTrain)	          
        print("dataset: {:s}".format(imglist))
        print("number of samples: {:d}".format(len(arrImg)))
        for kk in range(len(arrImg)):        
            imgs.append(arrImg[kk])
            x1.append(arrX1[kk])
            x2.append(arrX2[kk])
            y1.append(arrY1[kk])
            y2.append(arrY2[kk])
    
    #import pdb; pdb.set_trace()
    pts1 = np.array(zip(x1, y1))
    pts2 = np.array(zip(x2, y2))
    return imglist, imgs, pts1, pts2


def Crop_York_Roi(arrCx, arrCy, hR = 64):
    
    X_train, Y_train, X_test, Y_test = Load_York_Data(1, 256, 256, False)
    
    if len(arrCx)!=len(X_test):
        print "data dimension not match: arrCnt vs X_test"     
        return

    X_test_roi, Y_test_roi = [], []

    for i in range(len(arrCx)):
        mask = Y_test[i]
        img = X_test[i]
        Rec = mask[yCnt-hR:yCnt+hR,xCnt-hR:xCnt+hR]
        small = img[yCnt-hR:yCnt+hR,xCnt-hR:xCnt+hR]
        X_test_roi.append(small)
        Y_test_roi.append(Rec)
    return X_test_roi, Y_test_roi


#imglist, imgs, pts1, pts2 = Load_Loc_Data(1, 128, 128, True)
#X_train, Y_train, X_test, Y_test = Load_York_Data(1, 128, 128, True, 64) 
#import pdb; pdb.set_trace()



