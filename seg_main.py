from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D, UpSampling2D, ZeroPadding2D, Flatten, Reshape, Permute
from keras.optimizers import SGD, Adam
from keras.regularizers import l2, activity_l2
from keras.activations import softmax
from keras.utils.layer_utils import print_summary
from keras import backend as K
import numpy as np
import json
import os, fnmatch, shutil, subprocess
from os.path import basename
import scipy.io as sio
import cv2, re, sys
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import theano.tensor as T
from keras.layers.normalization import BatchNormalization
from keras.models import model_from_json, load_model
import theano

from theano.sandbox.rng_mrg import MRG_RandomStreams
theano_rand = MRG_RandomStreams()
from keras.callbacks import CSVLogger

np.random.seed(2017)

from MICCAI_Data import Load_MICCAI_Data
from York_Data import Load_York_Data, Crop_York_Roi
from keras.preprocessing.image import ImageDataGenerator
from numpy.random import permutation

def  dice(y_true, y_pred):
    # Symbolically compute the intersection
    y_int = y_true*y_pred
    # Technically this is the negative of the Sorensen-Dice index. This is done for
    # minimization purposes
    return (2*K.sum(y_int) / (K.sum(y_true) + K.sum(y_pred)))


def  dice_loss(y_true, y_pred):

    return 1-dice(y_true, y_pred)


def save_model(model, index, cross=''):
    json_string = model.to_json()
    if not os.path.isdir('cache'):
        os.mkdir('cache')
    json_name = 'architecture' + str(index) + cross + '.json'
    weight_name = 'model_weights' + str(index) + cross + '.h5'
    open(os.path.join('cache', json_name), 'w').write(json_string)
    model.save_weights(os.path.join('cache', weight_name), overwrite=True)


def read_model(index, cross=''):
    json_name = 'architecture' + str(index) + cross + '.json'
    weight_name = 'model_weights' + str(index) + cross + '.h5'
    model = model_from_json(open(os.path.join('cache', json_name)).read())  
    model.load_weights(os.path.join('cache', weight_name))
    return model

def load_york(C,H,W,bROI=False,hR=64):
    return Load_York_Data(C,H,W,bROI,hR)

def load_york_roi(arrCx, arrCy, hR=64):
    return Crop_York_Roi(arrCx, arrCy, hR)

def load_miccai(C,H,W,bROI=False):
    return Load_MICCAI_Data(C,H,W,bROI,hR)


from keras.models import Model
from keras.layers import Input, merge, Convolution2D, MaxPooling2D, UpSampling2D


def get_unet():
    inputs = Input((input_C, input_H, input_W))
    conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(inputs)
    conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(pool1)
    conv2 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(pool2)
    conv3 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv3)
    pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

    conv4 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(pool3)
    conv4 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(conv4)
    pool4 = MaxPooling2D(pool_size=(2, 2))(conv4)

    conv5 = Convolution2D(512, 3, 3, activation='relu', border_mode='same')(pool4)
    conv5 = Convolution2D(512, 3, 3, activation='relu', border_mode='same')(conv5)

    up6 = merge([UpSampling2D(size=(2, 2))(conv5), conv4], mode='concat', concat_axis=1)
    conv6 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(up6)
    conv6 = Convolution2D(256, 3, 3, activation='relu', border_mode='same')(conv6)

    up7 = merge([UpSampling2D(size=(2, 2))(conv6), conv3], mode='concat', concat_axis=1)
    conv7 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(up7)
    conv7 = Convolution2D(128, 3, 3, activation='relu', border_mode='same')(conv7)

    up8 = merge([UpSampling2D(size=(2, 2))(conv7), conv2], mode='concat', concat_axis=1)
    conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(up8)
    conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv8)

    up9 = merge([UpSampling2D(size=(2, 2))(conv8), conv1], mode='concat', concat_axis=1)
    conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(up9)
    conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv9)
    conv9 = BatchNormalization()(conv9)  
    conv10 = Convolution2D(1, 3, 3, activation='sigmoid', border_mode='same')(conv9)

    model = Model(input=inputs, output=conv10)

    return model



def train_aug(epoch_num, save_model_name='_1ch', prev_mod_num = None):
    ## Load X_train and Y_train here

    print 'loading data:'
    X_train_org, Y_train_org, X_test, Y_test = load_york(input_C,input_H,input_W,bROI,hR)
    #X_train_org, Y_train_org, X_test, Y_test = load_miccai(input_C,input_H,input_W,bROI,hR)

    print "Training Samples:"
    print X_train_org.shape, Y_train_org.shape   

    bNum = 16

    
    if prev_mod_num is not None:
        print 'loading model:'
        kk = prev_mod_num
        model = read_model(kk, save_model_name)
    else:
        print 'build model:'
        kk = -1
        model = get_unet() 

    print_summary(model.layers)

    #opt = SGD(lr=0.0001, decay=1e-6, momentum=0.90, nesterov=True)
    opt = Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    model.compile(loss=dice_loss, optimizer=opt, metrics=[dice])


    # this is the augmentation configuration we will use for training
    datagen = ImageDataGenerator(
	rotation_range=10.0,
	width_shift_range=0.1,
	height_shift_range=0.1,
        vertical_flip = True,
	horizontal_flip=False)



    for i in range(epoch_num):

        print "epoch: ", i+kk+1

        csv_logger = CSVLogger('training_new.log')

        perm = permutation(len(X_train_org))
        nVal = int(len(X_train_org)*0.15)
        X_val = X_train_org[perm[:nVal]]
        Y_val = Y_train_org[perm[:nVal]]
      
        X_train =  X_train_org[perm[nVal:]]
        Y_train =  Y_train_org[perm[nVal:]]

        print "Training Samples:"
        print X_train.shape, Y_train.shape, X_val.shape, Y_val.shape  

        # fits the model on batches with real-time data augmentation:
        model.fit_generator(datagen.flow(X_train, Y_train, batch_size=16),
                    samples_per_epoch=len(Y_train), 
		    nb_epoch=1, 
		    verbose=1,
                    validation_data = datagen.flow(X_val, Y_val, batch_size=16),
                    nb_val_samples = len(Y_val),
		    callbacks=[csv_logger])



        save_model(model, i+kk+1, save_model_name)


def train(epoch_num, save_model_name='_1ch', prev_mod_num = None):
    ## Load X_train and Y_train here

    print 'loading data:'
    X_train, Y_train, X_test, Y_test = load_york(input_C,input_H,input_W,bROI,hR)
    #X_train, Y_train, X_test, Y_test = load_miccai(input_C,input_H,input_W,bROI,hR)

    print "Training Samples:"
    print X_train.shape, Y_train.shape   

    bNum = 16
    if prev_mod_num is not None:
        print 'loading model:'
        kk = prev_mod_num
        model = read_model(kk, save_model_name)
    else:
        kk = -1
        model = get_unet()        
    print_summary(model.layers)    
    opt = Adam(lr=0.01, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    model.compile(loss=dice_loss, optimizer=opt, metrics=[dice])
    #model.compile(loss='binary_crossentropy', optimizer=opt, metrics=[dice])


    print "Training Samples:"
    print X_train.shape, Y_train.shape   

    for i in range(epoch_num):

        print "epoch: ", i+kk+1

        csv_logger = CSVLogger('training_progress.log')

        history = model.fit(X_train, Y_train, batch_size=bNum, nb_epoch=1, verbose=1, shuffle=True, validation_split=0.15, callbacks=[csv_logger])
        
        save_model(model, i+kk+1, save_model_name)

        
def test(mod_num, load_model_name='_1ch', loc_model = 'saved_final.model'):

    arrCx, arrCy = Output_Cnts(loc_model)
    
    X_test, Y_test = load_york_roi(input_C,input_H,input_W,bROI,hR,arrCx,arrCy)

    print "start perform testing"
    print "Testing Samples:"
    print X_test.shape, Y_test.shape



    for kk in mod_num:

	import time
	st = int(round(time.time() * 1000))
        print "mod_num: ", kk
        model = read_model(kk, load_model_name)
	THRESH = 0.5
	y_pred = model.predict(X_test) 
	label = Y_test
	mask = np.where(y_pred > THRESH, 1, 0)
	comb = mask+label
	L1 = np.sum(comb==1)
	L2 = np.sum(comb==2)
	IoU = L2*1.0/(L1+L2)  #conver to float first
        DM = L2*2.0/(L1+L2*2.0)
        #for i in range(len(y_pred)):

        if 1 == 1: #reserve for later use
            bIoU = False
            bPrint = True
	    IoU1 = 0
	    IoU2 = 0
            arrIoU1 = []
            arrIoU2 = []
            totalIoU1 = 0
            totalIoU2 = 0

	    num_images = len(y_pred)
	    for j in range(num_images):
		label = Y_test[j]
		mask = np.where(y_pred[j] > THRESH, 1, 0)
		comb = mask+label
		L1 = np.sum(comb==1)
		L2 = np.sum(comb==2)
                if bIoU:
		    accu1 = L2*1.0/(L1+L2)  #conver to float first
                else:
                    accu1 = L2*2.0/(L1+L2*2.0) 

                    
		IoU1 = IoU1 + accu1
		arrIoU1.append(accu1)
		totalIoU1 = totalIoU1 + accu1   

		mask = mask.reshape(mask.shape[1:3]) 
		img1 = cv2.convertScaleAbs(mask)             
	 
		label = label.reshape(label.shape[1:3]) 

		cnts, contours, hrchs = cv2.findContours(img1,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
		maxA = 0 
		tmpCtr = np.zeros((1,2),np.int32)
		for ctr in contours:
		    area = cv2.contourArea(ctr)
		    if area >= maxA:
		        maxA = area
		        tmpCtr = ctr 
	    
		mask1 = np.zeros_like(img1, dtype="uint8")
		cv2.fillPoly(mask1, [tmpCtr], 1)
		copy = mask1
		mask1 = mask1*mask
     
		comb = mask1+label
		L1 = np.sum(comb==1)
		L2 = np.sum(comb==2)

                if bIoU: 
	            accu2 = L2*1.0/(L1+L2)  #conver to float first
                else:
                    accu2 = L2*2.0/(L1+L2*2.0) 

		IoU2 = IoU2 + accu2
		arrIoU2.append(accu2)
		totalIoU2 = totalIoU2 + accu2


            #if bPrint:
	    print("bf post-processing IoU={:f}".format(IoU1*1.0/num_images))
	    print("af post-processing IoU={:f}".format(IoU2*1.0/num_images))


            diff = int(round(time.time() * 1000))-st
            if bPrint:
                print("total image = {:d} total time = {:f} average time = {:f}".format(num_images, diff, diff*1.0/num_images)) 
                print np.std(arrIoU1), np.std(arrIoU2)
                print("total images = {:d}, mean value = {:f}, std value = {:f}".format(num_images, np.mean(arrIoU2), np.std(arrIoU2)))
            

bROI = False
hR = 64
input_H = 128
input_W = 128
input_C = 1

seg_M = 'full_dm'
loc_M = 'saved_50.model'

train(50,seg_M)
test(range(49,50,1),seg_M,loc_M) 



