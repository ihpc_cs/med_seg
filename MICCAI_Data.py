import dicom, lmdb, cv2, re, sys
import os, fnmatch, shutil, subprocess
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
np.random.seed(2017)


SAX_SERIES = {
    # challenge val
    "SC-HF-I-5": "0156","SC-HF-I-6": "0180","SC-HF-I-7": "0209","SC-HF-I-8": "0226","SC-HF-NI-7": "0523","SC-HF-NI-11": "0270","SC-HF-NI-31": "0401","SC-HF-NI-33": "0424","SC-HYP-6": "0767","SC-HYP-7": "0007",
            "SC-HYP-8": "0796","SC-HYP-37": "0702","SC-N-5": "0963","SC-N-6": "0984","SC-N-7": "1009",
    # challenge test
    "SC-HF-I-9": "0241","SC-HF-I-10": "0024","SC-HF-I-11": "0043","SC-HF-I-12": "0062","SC-HF-NI-12": "0286","SC-HF-NI-13": "0304","SC-HF-NI-14": "0331","SC-HF-NI-15": "0359","SC-HYP-9": "0003","SC-HYP-10": "0579",
            "SC-HYP-11": "0601","SC-HYP-12": "0629", "SC-N-9": "1031", "SC-N-10": "0851","SC-N-11": "0878",
    # challenge train
    "SC-HF-I-1": "0004","SC-HF-I-2": "0106","SC-HF-I-4": "0116","SC-HF-I-40": "0134","SC-HF-NI-3": "0379","SC-HF-NI-4": "0501","SC-HF-NI-34": "0446","SC-HF-NI-36": "0474","SC-HYP-1": "0550","SC-HYP-3": "0650",
            "SC-HYP-38": "0734","SC-HYP-40": "0755","SC-N-2": "0898","SC-N-3": "0915","SC-N-40": "0944",
}

SUNNYBROOK_ROOT_PATH = "/home/yangx/xlyang/MyWork/input/LV/"

TRAIN_CONTOUR_PATH = os.path.join(SUNNYBROOK_ROOT_PATH,
                            "ContoursPart0",
                            "trn_tst")

TEST_CONTOUR_PATH = os.path.join(SUNNYBROOK_ROOT_PATH,
                            "ContoursPart0",
                            "val")

TRAIN_IMG_PATH = os.path.join(SUNNYBROOK_ROOT_PATH,
                        "challenge_combine")

def shrink_case(case):
    toks = case.split("-")
    def shrink_if_number(x):
        try:
            cvt = int(x)
            return str(cvt)
        except ValueError:
            return x
    return "-".join([shrink_if_number(t) for t in toks])

class Contour(object):
    def __init__(self, ctr_path):
        self.ctr_path = ctr_path
        match = re.search(r"/([^/]*)/contours-manual/IRCCI-expert/IM-0001-(\d{4})-icontour-manual.txt", ctr_path)
        self.case = shrink_case(match.group(1))
        self.img_no = int(match.group(2))
    
    def __str__(self):
        return "<Contour for case %s, image %d>" % (self.case, self.img_no)
    
    __repr__ = __str__


def load_img(full_path):
    f = dicom.read_file(full_path)
    img = f.pixel_array.astype(np.int)
    return img

def load_contour_multi(contour, img_path, input_C, input_H, input_W, bROI = False, hR = 64):
    filename = "IM-%s-%04d.dcm" % (SAX_SERIES[contour.case], contour.img_no)
    full_path = os.path.join(img_path, contour.case, filename)
    #print full_path
    
    preName = full_path[:-8]
    nexName = full_path[-8:]
    cur = int(nexName[:4])
    base = cur / 20 * 20
    resi = cur % 20
    if resi == 0:
        base = base - 20
    

    input_offset = input_C/2

    img, label = [], []
    seq = []
    #Append the before images

    n = input_offset			
    while (n > 0):
        pos = resi - n
        if pos < 1:
            pos = (20 - n) + resi
        #print "before position : ", pos	
        imgBefore = load_img(preName+('%04d' %(pos+base))+'.dcm')
        seq.append(pos+base)
        #print pos + base
        img.append(imgBefore)					
        n = n-1

    #print "current position : ", j
    imgCurr = load_img(preName+('%04d' %(cur))+'.dcm')
    seq.append(cur)
    #print cur
    img.append(imgCurr)
		
    #append the after images
    n = 0
    while (n < input_offset):
        pos = resi + (n+1)
        if pos > 20:
            pos = pos - 20
        #print "after position : ", pos
        imgAfter = imgBefore = load_img(preName+('%04d' %(pos+base))+'.dcm')
        seq.append(pos+base)
        #print pos + base
        img.append(imgAfter)	
        n = n+1
	
    #print filename, seq	
    img = np.array(img)
    img = img.transpose(1,2,0)
    #import pdb; pdb.set_trace()

    ctrs = np.loadtxt(contour.ctr_path, delimiter=" ").astype(np.int)
    lab = ctrs 
    #import pdb; pdb.set_trace()
    #print contour.ctr_path

    mask = np.zeros(img.shape[:2])
    mask = mask.astype(np.uint8)
    cv2.fillPoly(mask,[lab],1)
    

    if bROI:

        xMin = np.min(lab[:,0])
        xMax = np.max(lab[:,0])
        yMin = np.min(lab[:,1])
        yMax = np.max(lab[:,1])  

        xCnt = (xMin+xMax)/2
        yCnt = (yMin+yMax)/2


        Rec = mask[yCnt-hR:yCnt+hR,xCnt-hR:xCnt+hR]
        small = img[yCnt-hR:yCnt+hR,xCnt-hR:xCnt+hR]
        img = small
        mask = Rec 




    img = img.astype('<u2')
    img = cv2.resize(img,(input_H,input_W))


    mask = cv2.resize(mask,(input_H,input_W)) 
    label.append(mask)

    img = img.astype(np.int64)  
    img_id = contour.ctr_path
  
    vMean = np.mean(img)
    vVar = np.std(img)
    img = (img - vMean)*1.0 / vVar


    if (input_C==1):
        img = np.expand_dims(img, axis=0)    
    else:
        img = img.transpose(2, 0, 1)  #[C,H,W]


    img = np.expand_dims(img, axis=0)
    label = np.expand_dims(label, axis=0)

    #return img_id, img, label, xCnt, yCnt
    return img, label


    
def get_all_contours(contour_path):
    contours = [os.path.join(dirpath, f)
        for dirpath, dirnames, files in os.walk(contour_path)
        for f in fnmatch.filter(files, 'IM-0001-*-icontour-manual.txt')]
    #print("Shuffle data")
    #np.random.shuffle(contours)
    print("Number of examples: {:d}".format(len(contours)))
    extracted = map(Contour, contours)
    return extracted

def export_all_contours(contours, img_path, input_C, input_H, input_W, bROI = False, hR = 64):
    
    counter_img = 0
    counter_label = 0
    #import pdb; pdb.set_trace()
    imgs, labels = [], []
    for idx,ctr in enumerate(contours):
        try:
            arrImg, arrLabel = load_contour_multi(ctr, img_path, input_C, input_H, input_W, bROI, hR)
            for k in xrange(len(arrImg)):
                img = arrImg[k]
                label = arrLabel[k] 
                imgs.append(img)
                labels.append(label)
            if idx % 1== 1:
                print ctr
                import pdb; pdb.set_trace()
                f,(ax1,ax2)=plt.subplots(1,2,sharey=True)
                ax1.imshow(img,cmap='gray')
                ax2.imshow(label*255+img,cmap='gray')
                plt.show()
        except IOError:
            import pdb; pdb.set_trace()
            continue
    
    return np.array(imgs), np.array(labels)


def Load_MICCAI_Data(input_C, input_H, input_W, bROI=False, hR=64):
    trn_ctrs = get_all_contours(TRAIN_CONTOUR_PATH)
    X_train, Y_train = export_all_contours(trn_ctrs, TRAIN_IMG_PATH, input_C, input_H, input_W, bROI, hR)
    tst_ctrs = get_all_contours(TEST_CONTOUR_PATH)
    X_test, Y_test = export_all_contours(tst_ctrs, TRAIN_IMG_PATH, input_C, input_H, input_W, bROI, hR)
    

    return X_train, Y_train, X_test, Y_test

#X_train, Y_train, X_test, Y_test = Load_MICCAI_Data(1, 128, 128, True, 64)
#import pdb; pdb.set_trace()


