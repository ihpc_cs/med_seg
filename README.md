Deep learning approaches for biomedical image segmentation. 

To run the codes, you have to do:

1) download York University CMR database and MICCAI 2009 LV Challenge database, and unzip in ./input folder

2) run "train_model('loc.model')" in loc_main.py to train and save localization model, e.g., loc.model

3) run "train(epoch,'seg_york.h5')" in seg_main.py to train and save segmentation model, e.g., seg_york.h5 for York CMR database

4) run "test_york('seg_york.h5') in seg_main.py to test and generate the results on testing subjects from York CMR database


Contact: yangxl@i2r.a-star.edu.sg