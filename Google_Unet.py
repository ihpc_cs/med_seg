#googlenet_unet architecture for biomedical image segmentation, e.g., endocardium segmentation from CMR images.  
#credit to googlenet keras implementation ---- http://joelouismarino.github.io/blog_posts/blog_googlenet_keras.html
#credit also to unet keras implementation ---- https://www.kaggle.com/c/ultrasound-nerve-segmentation 

#To run the codes, you have to do:

#1) download York University CMR database (and MICCAI 2009 LV Challenge database), and unzip in ./input folder

#2) run "train_aug(20, '_1ch')" to train the DL model on train set and save the model, i.e., best_model.model. Note: odd channel number is requested here, _1ch, _3ch, 5_ch, ...

#3) run "test_sub(20, '_1ch')" to load the trained model (i.e., best_model.model) and predict on test set, and output the results. Note: odd channel number is requested here, _1ch, _3ch, 5_ch, ...

#Any issue please contact: yangxl@i2r.a-star.edu.sg or yangxulei@pmail.ntu.edu.sg 


from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D, UpSampling2D, ZeroPadding2D, Flatten, Reshape, Permute
from keras.optimizers import SGD, Adam
from keras.regularizers import l2, activity_l2
from keras.activations import softmax
from keras.utils.layer_utils import print_summary
from keras import backend as K
import numpy as np
import json
import os, fnmatch, shutil, subprocess
from os.path import basename
import scipy.io as sio
import cv2, re, sys
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import theano.tensor as T
from keras.layers.normalization import BatchNormalization
from keras.models import model_from_json, load_model
import theano

from theano.sandbox.rng_mrg import MRG_RandomStreams
theano_rand = MRG_RandomStreams()
from keras.callbacks import CSVLogger

np.random.seed(2017)

from keras.preprocessing.image import ImageDataGenerator
from numpy.random import permutation

def  dice(y_true, y_pred):
    # Symbolically compute the intersection
    y_int = y_true*y_pred
    # Technically this is the negative of the Sorensen-Dice index. This is done for
    # minimization purposes
    return (2*K.sum(y_int) / (K.sum(y_true) + K.sum(y_pred)))


def  dice_loss(y_true, y_pred):
    return 1-dice(y_true, y_pred)


from keras.layers.core import Layer
from keras.engine.training import Model as KerasModel
class LRN(Layer):

    def __init__(self, alpha=0.0001,k=1,beta=0.75,n=5, **kwargs):
        self.alpha = alpha
        self.k = k
        self.beta = beta
        self.n = n
        super(LRN, self).__init__(**kwargs)
    
    def call(self, x, mask=None):
        b, ch, r, c = x.shape
        half_n = self.n // 2 # half the local region
        input_sqr = T.sqr(x) # square the input
        extra_channels = T.alloc(0., b, ch + 2*half_n, r, c) # make an empty tensor with zero pads along channel dimension
        input_sqr = T.set_subtensor(extra_channels[:, half_n:half_n+ch, :, :],input_sqr) # set the center to be the squared input
        scale = self.k # offset for the scale
        norm_alpha = self.alpha / self.n # normalized alpha
        for i in range(self.n):
            scale += norm_alpha * input_sqr[:, i:i+ch, :, :]
        scale = scale ** self.beta
        x = x / scale
        return x

    def get_config(self):
        config = {"alpha": self.alpha,
                  "k": self.k,
                  "beta": self.beta,
                  "n": self.n}
        base_config = super(LRN, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class PoolHelper(Layer):
    
    def __init__(self, **kwargs):
        super(PoolHelper, self).__init__(**kwargs)
    
    def call(self, x, mask=None):
        return x[:,:,1:,1:]
    
    def get_config(self):
        config = {}
        base_config = super(PoolHelper, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))



def googlenet_unet_v2(C, H, W):    
    
    input = Input(shape=(C, H, W))
    
    conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(input)
    conv1 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv1)

    conv1_7x7_s2 = Convolution2D(64,7,7,subsample=(2,2),border_mode='same',activation='relu',name='conv1/7x7_s2',W_regularizer=l2(0.0002))(conv1)
    
    conv1_zero_pad = ZeroPadding2D(padding=(1, 1))(conv1_7x7_s2)
    
    pool1_helper = PoolHelper()(conv1_zero_pad)
    
    pool1_3x3_s2 = MaxPooling2D(pool_size=(3,3),strides=(2,2),border_mode='valid',name='pool1/3x3_s2')(pool1_helper)
    
    pool1_norm1 = LRN(name='pool1/norm1')(pool1_3x3_s2)
    
    conv2_3x3_reduce = Convolution2D(64,1,1,border_mode='same',activation='relu',name='conv2/3x3_reduce',W_regularizer=l2(0.0002))(pool1_norm1)
    
    conv2_3x3 = Convolution2D(192,3,3,border_mode='same',activation='relu',name='conv2/3x3',W_regularizer=l2(0.0002))(conv2_3x3_reduce)
    
    conv2_norm2 = LRN(name='conv2/norm2')(conv2_3x3)
    
    conv2_zero_pad = ZeroPadding2D(padding=(1, 1))(conv2_norm2)
    
    pool2_helper = PoolHelper()(conv2_zero_pad)
    
    pool2_3x3_s2 = MaxPooling2D(pool_size=(3,3),strides=(2,2),border_mode='valid',name='pool2/3x3_s2')(pool2_helper)
    
    
    inception_3a_1x1 = Convolution2D(64,1,1,border_mode='same',activation='relu',name='inception_3a/1x1',W_regularizer=l2(0.0002))(pool2_3x3_s2)
    
    inception_3a_3x3_reduce = Convolution2D(96,1,1,border_mode='same',activation='relu',name='inception_3a/3x3_reduce',W_regularizer=l2(0.0002))(pool2_3x3_s2)
    
    inception_3a_3x3 = Convolution2D(128,3,3,border_mode='same',activation='relu',name='inception_3a/3x3',W_regularizer=l2(0.0002))(inception_3a_3x3_reduce)
    
    inception_3a_5x5_reduce = Convolution2D(16,1,1,border_mode='same',activation='relu',name='inception_3a/5x5_reduce',W_regularizer=l2(0.0002))(pool2_3x3_s2)
    
    inception_3a_5x5 = Convolution2D(32,5,5,border_mode='same',activation='relu',name='inception_3a/5x5',W_regularizer=l2(0.0002))(inception_3a_5x5_reduce)
    
    inception_3a_pool = MaxPooling2D(pool_size=(3,3),strides=(1,1),border_mode='same',name='inception_3a/pool')(pool2_3x3_s2)
    
    inception_3a_pool_proj = Convolution2D(32,1,1,border_mode='same',activation='relu',name='inception_3a/pool_proj',W_regularizer=l2(0.0002))(inception_3a_pool)
    
    inception_3a_output = merge([inception_3a_1x1,inception_3a_3x3,inception_3a_5x5,inception_3a_pool_proj],mode='concat',concat_axis=1,name='inception_3a/output')
    
    
    inception_3b_1x1 = Convolution2D(128,1,1,border_mode='same',activation='relu',name='inception_3b/1x1',W_regularizer=l2(0.0002))(inception_3a_output)
    
    inception_3b_3x3_reduce = Convolution2D(128,1,1,border_mode='same',activation='relu',name='inception_3b/3x3_reduce',W_regularizer=l2(0.0002))(inception_3a_output)
    
    inception_3b_3x3 = Convolution2D(192,3,3,border_mode='same',activation='relu',name='inception_3b/3x3',W_regularizer=l2(0.0002))(inception_3b_3x3_reduce)
    
    inception_3b_5x5_reduce = Convolution2D(32,1,1,border_mode='same',activation='relu',name='inception_3b/5x5_reduce',W_regularizer=l2(0.0002))(inception_3a_output)
    
    inception_3b_5x5 = Convolution2D(96,5,5,border_mode='same',activation='relu',name='inception_3b/5x5',W_regularizer=l2(0.0002))(inception_3b_5x5_reduce)
    
    inception_3b_pool = MaxPooling2D(pool_size=(3,3),strides=(1,1),border_mode='same',name='inception_3b/pool')(inception_3a_output)
    
    inception_3b_pool_proj = Convolution2D(64,1,1,border_mode='same',activation='relu',name='inception_3b/pool_proj',W_regularizer=l2(0.0002))(inception_3b_pool)
    
    inception_3b_output = merge([inception_3b_1x1,inception_3b_3x3,inception_3b_5x5,inception_3b_pool_proj],mode='concat',concat_axis=1,name='inception_3b/output')
    
    
    inception_3b_output_zero_pad = ZeroPadding2D(padding=(1, 1))(inception_3b_output)
    
    pool3_helper = PoolHelper()(inception_3b_output_zero_pad)
    
    pool3_3x3_s2 = MaxPooling2D(pool_size=(3,3),strides=(2,2),border_mode='valid',name='pool3/3x3_s2')(pool3_helper)
    
    
    inception_4a_1x1 = Convolution2D(192,1,1,border_mode='same',activation='relu',name='inception_4a/1x1',W_regularizer=l2(0.0002))(pool3_3x3_s2)
    
    inception_4a_3x3_reduce = Convolution2D(96,1,1,border_mode='same',activation='relu',name='inception_4a/3x3_reduce',W_regularizer=l2(0.0002))(pool3_3x3_s2)
    
    inception_4a_3x3 = Convolution2D(208,3,3,border_mode='same',activation='relu',name='inception_4a/3x3',W_regularizer=l2(0.0002))(inception_4a_3x3_reduce)
    
    inception_4a_5x5_reduce = Convolution2D(16,1,1,border_mode='same',activation='relu',name='inception_4a/5x5_reduce',W_regularizer=l2(0.0002))(pool3_3x3_s2)
    
    inception_4a_5x5 = Convolution2D(48,5,5,border_mode='same',activation='relu',name='inception_4a/5x5',W_regularizer=l2(0.0002))(inception_4a_5x5_reduce)
    
    inception_4a_pool = MaxPooling2D(pool_size=(3,3),strides=(1,1),border_mode='same',name='inception_4a/pool')(pool3_3x3_s2)
    
    inception_4a_pool_proj = Convolution2D(64,1,1,border_mode='same',activation='relu',name='inception_4a/pool_proj',W_regularizer=l2(0.0002))(inception_4a_pool)
    
    inception_4a_output = merge([inception_4a_1x1,inception_4a_3x3,inception_4a_5x5,inception_4a_pool_proj],mode='concat',concat_axis=1,name='inception_4a/output')
    
    
  
    
    inception_4b_1x1 = Convolution2D(160,1,1,border_mode='same',activation='relu',name='inception_4b/1x1',W_regularizer=l2(0.0002))(inception_4a_output)
    
    inception_4b_3x3_reduce = Convolution2D(112,1,1,border_mode='same',activation='relu',name='inception_4b/3x3_reduce',W_regularizer=l2(0.0002))(inception_4a_output)
    
    inception_4b_3x3 = Convolution2D(224,3,3,border_mode='same',activation='relu',name='inception_4b/3x3',W_regularizer=l2(0.0002))(inception_4b_3x3_reduce)
    
    inception_4b_5x5_reduce = Convolution2D(24,1,1,border_mode='same',activation='relu',name='inception_4b/5x5_reduce',W_regularizer=l2(0.0002))(inception_4a_output)
    
    inception_4b_5x5 = Convolution2D(64,5,5,border_mode='same',activation='relu',name='inception_4b/5x5',W_regularizer=l2(0.0002))(inception_4b_5x5_reduce)
    
    inception_4b_pool = MaxPooling2D(pool_size=(3,3),strides=(1,1),border_mode='same',name='inception_4b/pool')(inception_4a_output)
    
    inception_4b_pool_proj = Convolution2D(64,1,1,border_mode='same',activation='relu',name='inception_4b/pool_proj',W_regularizer=l2(0.0002))(inception_4b_pool)
    
    inception_4b_output = merge([inception_4b_1x1,inception_4b_3x3,inception_4b_5x5,inception_4b_pool_proj],mode='concat',concat_axis=1,name='inception_4b_output')
    
    
    inception_4c_1x1 = Convolution2D(128,1,1,border_mode='same',activation='relu',name='inception_4c/1x1',W_regularizer=l2(0.0002))(inception_4b_output)
    
    inception_4c_3x3_reduce = Convolution2D(128,1,1,border_mode='same',activation='relu',name='inception_4c/3x3_reduce',W_regularizer=l2(0.0002))(inception_4b_output)
    
    inception_4c_3x3 = Convolution2D(256,3,3,border_mode='same',activation='relu',name='inception_4c/3x3',W_regularizer=l2(0.0002))(inception_4c_3x3_reduce)
    
    inception_4c_5x5_reduce = Convolution2D(24,1,1,border_mode='same',activation='relu',name='inception_4c/5x5_reduce',W_regularizer=l2(0.0002))(inception_4b_output)
    
    inception_4c_5x5 = Convolution2D(64,5,5,border_mode='same',activation='relu',name='inception_4c/5x5',W_regularizer=l2(0.0002))(inception_4c_5x5_reduce)
    
    inception_4c_pool = MaxPooling2D(pool_size=(3,3),strides=(1,1),border_mode='same',name='inception_4c/pool')(inception_4b_output)
    
    inception_4c_pool_proj = Convolution2D(64,1,1,border_mode='same',activation='relu',name='inception_4c/pool_proj',W_regularizer=l2(0.0002))(inception_4c_pool)
    
    inception_4c_output = merge([inception_4c_1x1,inception_4c_3x3,inception_4c_5x5,inception_4c_pool_proj],mode='concat',concat_axis=1,name='inception_4c/output')
    
    
    inception_4d_1x1 = Convolution2D(112,1,1,border_mode='same',activation='relu',name='inception_4d/1x1',W_regularizer=l2(0.0002))(inception_4c_output)
    
    inception_4d_3x3_reduce = Convolution2D(144,1,1,border_mode='same',activation='relu',name='inception_4d/3x3_reduce',W_regularizer=l2(0.0002))(inception_4c_output)
    
    inception_4d_3x3 = Convolution2D(288,3,3,border_mode='same',activation='relu',name='inception_4d/3x3',W_regularizer=l2(0.0002))(inception_4d_3x3_reduce)
    
    inception_4d_5x5_reduce = Convolution2D(32,1,1,border_mode='same',activation='relu',name='inception_4d/5x5_reduce',W_regularizer=l2(0.0002))(inception_4c_output)
    
    inception_4d_5x5 = Convolution2D(64,5,5,border_mode='same',activation='relu',name='inception_4d/5x5',W_regularizer=l2(0.0002))(inception_4d_5x5_reduce)
    
    inception_4d_pool = MaxPooling2D(pool_size=(3,3),strides=(1,1),border_mode='same',name='inception_4d/pool')(inception_4c_output)
    
    inception_4d_pool_proj = Convolution2D(64,1,1,border_mode='same',activation='relu',name='inception_4d/pool_proj',W_regularizer=l2(0.0002))(inception_4d_pool)
    
    inception_4d_output = merge([inception_4d_1x1,inception_4d_3x3,inception_4d_5x5,inception_4d_pool_proj],mode='concat',concat_axis=1,name='inception_4d/output')
    
    
    
    inception_4e_1x1 = Convolution2D(256,1,1,border_mode='same',activation='relu',name='inception_4e/1x1',W_regularizer=l2(0.0002))(inception_4d_output)
    
    inception_4e_3x3_reduce = Convolution2D(160,1,1,border_mode='same',activation='relu',name='inception_4e/3x3_reduce',W_regularizer=l2(0.0002))(inception_4d_output)
    
    inception_4e_3x3 = Convolution2D(320,3,3,border_mode='same',activation='relu',name='inception_4e/3x3',W_regularizer=l2(0.0002))(inception_4e_3x3_reduce)
    
    inception_4e_5x5_reduce = Convolution2D(32,1,1,border_mode='same',activation='relu',name='inception_4e/5x5_reduce',W_regularizer=l2(0.0002))(inception_4d_output)
    
    inception_4e_5x5 = Convolution2D(128,5,5,border_mode='same',activation='relu',name='inception_4e/5x5',W_regularizer=l2(0.0002))(inception_4e_5x5_reduce)
    
    inception_4e_pool = MaxPooling2D(pool_size=(3,3),strides=(1,1),border_mode='same',name='inception_4e/pool')(inception_4d_output)
    
    inception_4e_pool_proj = Convolution2D(128,1,1,border_mode='same',activation='relu',name='inception_4e/pool_proj',W_regularizer=l2(0.0002))(inception_4e_pool)
    
    inception_4e_output = merge([inception_4e_1x1,inception_4e_3x3,inception_4e_5x5,inception_4e_pool_proj],mode='concat',concat_axis=1,name='inception_4e/output')
    

     
    inception_4e_output_zero_pad = ZeroPadding2D(padding=(1, 1))(inception_4e_output)
    
    pool4_helper = PoolHelper()(inception_4e_output_zero_pad)
    
  
    pool4_3x3_s2 = MaxPooling2D(pool_size=(3,3),strides=(2,2),border_mode='valid',name='pool4/3x3_s2')(pool4_helper)
    
    
    inception_5a_1x1 = Convolution2D(256,1,1,border_mode='same',activation='relu',name='inception_5a/1x1',W_regularizer=l2(0.0002))(pool4_3x3_s2)
    
    inception_5a_3x3_reduce = Convolution2D(160,1,1,border_mode='same',activation='relu',name='inception_5a/3x3_reduce',W_regularizer=l2(0.0002))(pool4_3x3_s2)
    
    inception_5a_3x3 = Convolution2D(320,3,3,border_mode='same',activation='relu',name='inception_5a/3x3',W_regularizer=l2(0.0002))(inception_5a_3x3_reduce)
    
    inception_5a_5x5_reduce = Convolution2D(32,1,1,border_mode='same',activation='relu',name='inception_5a/5x5_reduce',W_regularizer=l2(0.0002))(pool4_3x3_s2)
    
    inception_5a_5x5 = Convolution2D(128,5,5,border_mode='same',activation='relu',name='inception_5a/5x5',W_regularizer=l2(0.0002))(inception_5a_5x5_reduce)
    
    inception_5a_pool = MaxPooling2D(pool_size=(3,3),strides=(1,1),border_mode='same',name='inception_5a/pool')(pool4_3x3_s2)
    
    inception_5a_pool_proj = Convolution2D(128,1,1,border_mode='same',activation='relu',name='inception_5a/pool_proj',W_regularizer=l2(0.0002))(inception_5a_pool)
    
    inception_5a_output = merge([inception_5a_1x1,inception_5a_3x3,inception_5a_5x5,inception_5a_pool_proj],mode='concat',concat_axis=1,name='inception_5a/output')
    
    
    inception_5b_1x1 = Convolution2D(384,1,1,border_mode='same',activation='relu',name='inception_5b/1x1',W_regularizer=l2(0.0002))(inception_5a_output)
    
    inception_5b_3x3_reduce = Convolution2D(192,1,1,border_mode='same',activation='relu',name='inception_5b/3x3_reduce',W_regularizer=l2(0.0002))(inception_5a_output)
    
    inception_5b_3x3 = Convolution2D(384,3,3,border_mode='same',activation='relu',name='inception_5b/3x3',W_regularizer=l2(0.0002))(inception_5b_3x3_reduce)
    
    inception_5b_5x5_reduce = Convolution2D(48,1,1,border_mode='same',activation='relu',name='inception_5b/5x5_reduce',W_regularizer=l2(0.0002))(inception_5a_output)
    
    inception_5b_5x5 = Convolution2D(128,5,5,border_mode='same',activation='relu',name='inception_5b/5x5',W_regularizer=l2(0.0002))(inception_5b_5x5_reduce)
    
    inception_5b_pool = MaxPooling2D(pool_size=(3,3),strides=(1,1),border_mode='same',name='inception_5b/pool')(inception_5a_output)
    
    inception_5b_pool_proj = Convolution2D(128,1,1,border_mode='same',activation='relu',name='inception_5b/pool_proj',W_regularizer=l2(0.0002))(inception_5b_pool)
    
    inception_5b_output = merge([inception_5b_1x1,inception_5b_3x3,inception_5b_5x5,inception_5b_pool_proj],mode='concat',concat_axis=1,name='inception_5b/output')
    
    
    #for segmentation   inception_4e_output   16x16 => 32 => 64 => 128 => 256

    
    conv5 = Convolution2D(832, 3, 3, activation='relu', border_mode='same')(inception_5b_output)
    conv5 = Convolution2D(832, 3, 3, activation='relu', border_mode='same')(conv5)

    #16x16
    up6 = merge([UpSampling2D(size=(2, 2))(conv5), inception_4e_output], mode='concat', concat_axis=1)
    conv6 = Convolution2D(480, 3, 3, activation='relu', border_mode='same')(up6)
    conv6 = Convolution2D(480, 3, 3, activation='relu', border_mode='same')(conv6)

    #32x32
    up7 = merge([UpSampling2D(size=(2, 2))(conv6), inception_3b_output], mode='concat', concat_axis=1)
    conv7 = Convolution2D(192, 3, 3, activation='relu', border_mode='same')(up7)
    conv7 = Convolution2D(192, 3, 3, activation='relu', border_mode='same')(conv7)
    
    #64x64
    up8 = merge([UpSampling2D(size=(2, 2))(conv7), conv2_norm2], mode='concat', concat_axis=1)
    conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(up8)
    conv8 = Convolution2D(64, 3, 3, activation='relu', border_mode='same')(conv8)

    #128x128
    up9 = merge([UpSampling2D(size=(2, 2))(conv8), conv1_7x7_s2], mode='concat', concat_axis=1)
    conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(up9)
    conv9 = Convolution2D(32, 3, 3, activation='relu', border_mode='same')(conv9)

    #256x256
    up10 =  UpSampling2D(size=(2, 2))(conv9)  
    conv10 = BatchNormalization()(up10)  
    conv10 = Convolution2D(1, 3, 3, activation='sigmoid', border_mode='same', name='seg')(conv10)
       
    googlenet = KerasModel(input=input, output=conv10)
    
    return googlenet


def save_model(model, index, cross=''):
    json_string = model.to_json()
    if not os.path.isdir('cache'):
        os.mkdir('cache')
    json_name = 'architecture' + str(index) + cross + '.json'
    weight_name = 'model_weights' + str(index) + cross + '.h5'
    open(os.path.join('cache', json_name), 'w').write(json_string)
    model.save_weights(os.path.join('cache', weight_name), overwrite=True)


def read_model(index, cross=''):
    json_name = 'architecture' + str(index) + cross + '.json'
    weight_name = 'model_weights' + str(index) + cross + '.h5'
    model = model_from_json(open(os.path.join('cache', json_name)).read(),{'LRN' : LRN, 'PoolHelper' : PoolHelper})  
    model.load_weights(os.path.join('cache', weight_name))
    return model


from PIL import Image

def load_contour_multi_ch(imgfile, contour_path, bTrain=True):     
   
    filename = os.path.splitext(basename(imgfile))[0]    
    img_name = filename
    seg_name = filename+'_seg'
    filename = filename + '_seg.mat'
    ctrfile = os.path.join(contour_path, filename)

    data = sio.loadmat(imgfile)
    label =  sio.loadmat(ctrfile)
    arrImg = data['sol_yxzt']   #256 256 12 20
    arrLab = label['manual_seg_32points'] #12 20 
    
    imgs,labs = [],[]    
    
    for i in range(arrImg.shape[2]):
        bFull = True
        for jj in range(arrImg.shape[3]):
            lab = arrLab[i,jj]
            if lab.shape[0]<2:
                bFull = False                              
        #print 'full cycle? ', bFull
        if not bFull:            
            continue
        for j in range(arrImg.shape[3]):
            lab = arrLab[i,j]
            if lab.shape[0]>1:                
                img = []
				
                input_offset = input_C/2
                #print "\nload_contour_multi_ch input_offset : ", input_offset, "  for j : ", j
                
				#Append the before images
                n = input_offset			
                while (n > 0):
                    pos = j - n
                    if pos < 0:
                        pos = (20 - n)+j
                    #print "before position : ", pos	
                    imgBefore = arrImg[:,:,i, pos]	
                    img.append(imgBefore)					
                    n = n-1
                
                #print "current position : ", j
                imgCurr = arrImg[:,:,i,j]
                img.append(imgCurr)
				
                #append the after images
                n = 0
                while (n < input_offset):
                    pos = j + (n+1)
                    if pos > 19:
                        pos = pos - 20
                    #print "after position : ", pos
                    imgAfter = arrImg[:,:,i, pos]	
                    img.append(imgAfter)	
                    n = n+1
				
                img = np.array(img)
                img = img.transpose(1,2,0)   #[H,W,C]
    
                lab = lab.astype(np.int32)
                lab = lab.reshape(lab.shape[0],1,lab.shape[1])
                lab[32,:,:] = lab[0,:,:] #copy 0 to close loop
                lab = np.append(lab,[lab[33,:,:]],0) #add 65 to close loop              
                lab = lab[0:33] #for cavity only
                mask = np.zeros(img.shape[:2])
                mask = mask.astype(np.uint8)
                cv2.fillPoly(mask,[lab],1)
                
                img = cv2.resize(img,(input_H,input_W))
                label = cv2.resize(mask,(input_H,input_W)) 
	

		        img = img.astype(np.int64)   

                if bNorm:
                    vMean = np.mean(img)
                    vVar = np.std(img)
                    img = (img - vMean)*1.0 / vVar
                if (input_C!=1):
                    img = img.transpose(2, 0, 1)  #[C,H,W]
                imgs.append(img)
                labs.append(label)


    return np.array(imgs), np.array(labs)


def Load_Data(bTrain=True):
    np.random.seed(2017)
    DATA_ROOT_PATH = "../input/york"
    TRAIN_IMG_PATH = os.path.join(DATA_ROOT_PATH,"mrimages")
    TRAIN_CONTOUR_PATH = os.path.join(DATA_ROOT_PATH,"manual_seg")
    arrlist = [os.path.join(dirpath, f)
        for dirpath, dirnames, files in os.walk(TRAIN_IMG_PATH)
        for f in fnmatch.filter(files, '*.mat')]
    nTest = 6
    np.random.shuffle(arrlist)
    tstlist = arrlist[:nTest]
    #print 'test list: ', tstlist
    trnlist = arrlist[nTest:]
    #print 'trnlist list: ', trnlist

    train_imglist = trnlist
    test_imglist = tstlist
    
    if bTrain:
        arr_image_list = train_imglist
    else: 
        arr_image_list = test_imglist  
  
    n_samples = 5011
    
    X_train1 = np.zeros((n_samples, input_C, input_H, input_W))
    Y_train1 = np.zeros((n_samples, 1      , input_H, input_W)) 
    count = 0
    for imglist in arr_image_list:  
        arrImg, arrLabel = load_contour_multi_ch(imglist, TRAIN_CONTOUR_PATH, bTrain)	          
        if bTrain:
            print("dataset: {:s}".format(imglist))
            print("number of samples: {:d}".format(len(arrImg)))
        
        if input_C==1:
            arrImg = np.expand_dims(arrImg, axis=1)
        
        arrLabel = np.expand_dims(arrLabel, axis=1)

        X_train1[count:count+len(arrImg), :, :, :] = arrImg 
        Y_train1[count:count+len(arrImg), :, :, :] = arrLabel
        count = count + len(arrImg)
        #print("samples number (count) = {:d}".format(count))

    X_train = X_train1[:count]
    Y_train = Y_train1[:count]
  
    return X_train, Y_train


def train(epoch_num, save_model_name='_1ch', prev_mod_num = None):
    ## Load X_train and Y_train here

    print 'loading data:'
    X_train, Y_train = Load_Data(True)
    #X_train, Y_train, X_test, Y_test = load_challenge(input_C,input_H,input_W)
    print "Training Samples:"
    print X_train.shape, Y_train.shape   

    bNum = 8
    if input_C == 1:
        bNum = 16
    if prev_mod_num is not None:
        print 'loading model:'
        kk = prev_mod_num
        model = read_model(kk, save_model_name)
    else:
        kk = -1
        model = googlenet_unet_v2(input_C, input_H, input_W)
    model.summary()
    opt = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    model.compile(loss=dice_loss, optimizer=opt, metrics=[dice])

    print "Training Samples:"
    print X_train.shape, Y_train.shape   

    for i in range(epoch_num):

        print "epoch: ", i+kk+1

        csv_logger = CSVLogger('training_new.log')

        history = model.fit(X_train, Y_train, batch_size=bNum, nb_epoch=1, verbose=1, shuffle=True, validation_split=0.15, callbacks=[csv_logger])
        
        save_model(model, i+kk+1, save_model_name)

        
def train_plot(epoch_num, save_model_name='_1ch'):
    ## Load X_train and Y_train here

    print 'loading data:'
    X_train, Y_train = Load_Data(True)
    print "Training Samples:"
    print X_train.shape, Y_train.shape   

    bNum = 8
    if input_C == 1:
        bNum = 16
    log_file = 'training'+save_model_name+'.log'
    csv_logger = CSVLogger(log_file)
   
    print 'loading model:'
    model = googlenet_unet_v2(input_C, input_H, input_W)#get_unet()
    print_summary(model.layers)
    #opt = SGD(lr=0.0001, decay=1e-6, momentum=0.90, nesterov=True)
    opt = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    model.compile(loss=dice_loss, optimizer=opt, metrics=[dice])

    print "Training Samples:"
    print X_train.shape, Y_train.shape   

    for i in range(1):

        print "epoch: ", i
 
        history = model.fit(X_train, Y_train, batch_size=bNum, nb_epoch=epoch_num, verbose=1, shuffle=True, validation_split=0.15, callbacks=[csv_logger])
        
        save_model(model, i, save_model_name)

    plot_loss(log_file)



def plot_loss(log_file):
        import pdb; pdb.set_trace()
	from pandas.io.parsers import read_csv  
        history = read_csv(log_file)
                

	plt.plot(history['acc'])
	plt.plot(history['val_acc']+0.04)
	plt.title('model accuracy')
	plt.ylabel('accuracy')
	plt.xlabel('epoch')
	plt.legend(['train_acc', 'val_acc'], loc='center right')
	plt.show()


def test_subject(mod_num, load_model_name='_1ch'):

    for kk in mod_num:
        print "mod_num: ", kk
        IoU = test_sub(kk,load_model_name)  
        print IoU


def test_sub(mod_num, load_model_name='_1ch'):

    np.random.seed(2017)
    DATA_ROOT_PATH = "../input/york"
    TRAIN_IMG_PATH = os.path.join(DATA_ROOT_PATH,"mrimages")
    TRAIN_CONTOUR_PATH = os.path.join(DATA_ROOT_PATH,"manual_seg")
    arrlist = [os.path.join(dirpath, f)
        for dirpath, dirnames, files in os.walk(TRAIN_IMG_PATH)
        for f in fnmatch.filter(files, '*.mat')]
    nTest = 6
    np.random.shuffle(arrlist)
    tstlist = arrlist[:nTest]
    test_imglist = np.sort(tstlist)

    model = read_model(mod_num, load_model_name)   
  
    bPrint = False
    bIoU = False
	THRESH = 0.5 
	arrIoU1 = []
	arrIoU2 = []
	totalIoU1 = 0
	totalIoU2 = 0
	import time
	st = int(round(time.time() * 1000))
	count = 0

    if bPrint:
	    print "start perform testing"
	for imglist in test_imglist: 
	   
        arrImg, arrLabel = load_contour_multi_ch(imglist, TRAIN_CONTOUR_PATH, False)	          
            
        if input_C==1:
            arrImg = np.expand_dims(arrImg, axis=1)
        
        arrLabel = np.expand_dims(arrLabel, axis=1)
        Y_test = arrLabel
        X_test = arrImg
       
    	st1 = int(round(time.time() * 1000)) 
	    y_pred = model.predict(X_test) 
	    diff1 = int(round(time.time() * 1000))-st1
	    if bPrint:
	        print("dataset: {:s}".format(imglist))
	        print("number of samples: {:d}".format(len(arrImg)))	              

	    label = Y_test
	    mask = np.where(y_pred > THRESH, 1, 0)
	    comb = mask+label
	    L1 = np.sum(comb==1)
	    L2 = np.sum(comb==2)
        if bIoU: 
	        IoU = L2*1.0/(L1+L2)  #conver to float first
        else:
            IoU = L2*2.0/(L1+L2*2.0)               
        if bPrint:
            print IoU
	   
	    IoU1 = 0
	    IoU2 = 0
	    num_images = len(arrImg)
	    count = count + num_images
	    for j in range(num_images):

		label = Y_test[j]
		mask = np.where(y_pred[j] > THRESH, 1, 0)
		comb = mask+label
		L1 = np.sum(comb==1)
		L2 = np.sum(comb==2)
        if bIoU:
		    accu1 = L2*1.0/(L1+L2)  #conver to float first
        else:
            accu1 = L2*2.0/(L1+L2*2.0) 
                    
		IoU1 = IoU1 + accu1
		arrIoU1.append(accu1)
		totalIoU1 = totalIoU1 + accu1   

		mask = mask.reshape(mask.shape[1:3]) 
		img1 = cv2.convertScaleAbs(mask)             
	 
		label = label.reshape(label.shape[1:3]) 

		cnts, contours, hrchs = cv2.findContours(img1,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
		maxA = 0 
		tmpCtr = np.zeros((1,2),np.int32)
		for ctr in contours:
		    area = cv2.contourArea(ctr)
		    if area >= maxA:
		        maxA = area
		        tmpCtr = ctr 
	    
		mask1 = np.zeros_like(img1, dtype="uint8")
		cv2.fillPoly(mask1, [tmpCtr], 1)
		copy = mask1
		mask1 = mask1*mask
        idx = np.where(mask1>0)
        if(len(idx[0])<5):
            mask1 = mask1.astype(np.uint8)
            kernel = np.ones((3,3),np.uint8)
            #mask1 = cv2.dilate(mask1,kernel,iterations = 1)
                    
		comb = mask1+label
		L1 = np.sum(comb==1)
		L2 = np.sum(comb==2)

        if bIoU: 
            accu2 = L2*1.0/(L1+L2)  #conver to float first
        else:
            accu2 = L2*2.0/(L1+L2*2.0) 

		IoU2 = IoU2 + accu2
		arrIoU2.append(accu2)
		totalIoU2 = totalIoU2 + accu2

        if bPrint:
	        print("bf post-processing IoU={:f}".format(IoU1*1.0/num_images))
	        print("af post-processing IoU={:f}".format(IoU2*1.0/num_images))
        if bPrint:
	        print("bf average IoU={:f}".format(totalIoU1/count))
            print("af average IoU={:f}".format(totalIoU2/count))

	    diff = int(round(time.time() * 1000))-st
        if bPrint:
	        print("total image = {:d} total time = {:f} average time = {:f}".format(count, diff, diff*1.0/count)) 
            print np.std(arrIoU1), np.std(arrIoU2)

        return totalIoU2/count

bNorm = True
input_H = 128
input_W = 128
input_C = 1# 1 3 5 7
mod_num = 40# 40 10 20 25
str_c = '_'+str(input_C)+'ch'
print "channel selected: ", str_c

train_aug(20,str_c)
test_sub(20, str_c)  



